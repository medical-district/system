

******

This module is provided on behalf of the\
Rhetorically Elloquent Advanced Development Moves Ensemble\
for the pursuit of intergalactic mercantile diplomas.

******

# mercantile



**literature**
```literature
This is a database that exists while a browser page is open.
It doesn't interface with cookies, localStorage, etc. 
```

******

```tcsh
[tcsh] yarn add mercantile
```

******

**javascript**
```javascript

import { make_system } from 'mercantile'

function is_equal (v1, v2) {
	if (v1 === v2) {
		return true;
	}
	throw new Error (`v1 '${ v1 }' does not equal v2 '${ v2 }'`)
}


/*
	These functions are called only once,
	in this sequence, after "make_system" is called:
	
		warehouse
		once_at.start
*/
const system = await make_system ({
	warehouse: async function () {
		console.log ("warehouse function called")
		
		return {
			status: 7
		}
	},

	activities: {
		async activity_1 (
			{ change, warehouse }, 
			{ numeral }
		) {
			let status = (await warehouse ()).status			
			await change ("status", status * numeral)
			
			return "good";
		}
	},
	
	once_at: {
		async start ({ activities, warehouse }) {
			console.log ("start function called")					
		}
	}			
})	

/*
	system monitoring:
	
	notes:
		The monitor function is called once when
		setup, with "inaugural" == true.
	
		The monitor function is called every time
		an activity calls "change", such as:
		
			await change ("status", 998)
*/		
const monitor = system.monitor (({ inaugural, field }) => {
	const warehouse = system.warehouse ()

	console.log ('monitor function', { inaugural, field, warehouse })
})


//
// After subscribing, the subscriber count is 1
is_equal (system.monitor_count (), 1)


//
// activities
const activity_1_proceeds = await system.activities.activity_1 ({ numeral: 10 })
is_equal (activity_1_proceeds, "good") 
is_equal (system.warehouse ().status, 70) 


monitor.stop ()
is_equal (system.monitor_count (), 0)
```

******

## vue 3 (with vite) usage
### '@/warehouses/palette/index.js'
```javascript

import { make_system } from 'mercantile'

export let palette_system;
export const create_palette_system = async function () {
	palette_system = await make_system ({
		warehouse: async function () {	
			const name = "1"
			const palettes = Object.freeze ({
				"dark": Object.freeze ({
					1: "#111",
					2: "#eee"
				}),
				"light": Object.freeze ({
					1: "#eee",
					2: "#111"
				})
			})
			
			return {
				name,
				palette: palettes [ name ],
				palettes
			}
		},
		
		activities: {
			/*
				await palette_system.activities.change_palette ("light")
			*/
			async change_palette ({ change, warehouse }, name) {		
				let palettes = (await warehouse ()).palettes
				
				await change ("palette", palettes [ name ])
				await change ("name", name)		
			}
		},
		
		once_at: {
			async start () {}
		}			
	})
})
```

******

### '@/src/main.js'
```javascript
import { createApp } from 'vue'
import { create_palette_system } from '@/warehouses/palette'

await Promise.all ([
	create_palette_system ()
])

// const app = createApp ()
// app.mount ('#app')

```
******

### '@/decor/item 1.vue'
```javascript
import { palette_system } from '@/warehouses/palette'
	
	
/*
	sequence:
		beforeCreate
		data
		created
		beforeMount
		mounted
		
		beforeUnmount
		unmounted
*/
export default {
	data () {
		return {
			palette: palette_system.warehouse ("palette")
		}
	},
	created () {		
		this.palette_system_monitor = palette_system.monitor (({ inaugeral, field }) => {
			this.palette = palette_system.warehouse ("palette")
			
			
		})		
	},
	beforeUnmount () {
		this.palette_system_monitor.stop ()
	}
}
```

******
