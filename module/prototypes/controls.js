

import cloneDeep from 'lodash/cloneDeep'

export function controls (parameters) {
	const this_system = this;

	function controls () {
		this.change = async function (field, value) {
			if (this_system.film >= 1) { 
				console.log ("system: controls change function called", { field, value }) 
			}
					
			this_system.warehouse [ field ] = value;
					
			await this_system.change_occurred ({ field })
			return
		}
	}
	controls.prototype.warehouse = function (fieldS) {
		if (typeof fieldS === "string") {
			return cloneDeep (this_system.warehouse [ fieldS ])
		}
		
		return cloneDeep (this_system.warehouse)
	}
	
	/*
		{ monitor, glimpse, observe }
	*/
	controls.prototype.monitor = function (procedure) {
		if (this_system.film >= 1) { console.log ("system: monitor was called") }
		
		this_system.monitors.push (procedure)
		
		procedure ({
			inaugural: true
		})
		
		return {
			stop: function () {
				this_system.stop_monitoring (procedure)
			}
		}		
	}
	controls.prototype.monitor_count = function (procedure) {
		return this_system.monitors.length
	}	
	controls.prototype.demo = function () {
		this_system.demo ()
	}
	
	const I = new controls ()
	
	return I;
}