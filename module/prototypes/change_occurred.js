


export async function change_occurred ({ field }) {
	const monitors = this.monitors;
	
	for (let S = 0; S < monitors.length; S++) {
		const monitor = monitors [S]
		monitor ({
			inaugural: false,
			field
		})
	}
}