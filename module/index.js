



import cloneDeep from 'lodash/cloneDeep'

import { change_occurred } from './prototypes/change_occurred'
import { stop_monitoring } from './prototypes/stop_monitoring'
import { controls } from './prototypes/controls'
import { demo }	from './prototypes/demo'

const has_field = function (this_object, field) {	
	if (Object.prototype.hasOwnProperty.call (this_object, field)) {
		return true;
	}
	
	return false;
}

function system () {
	this.monitors = []	
	this.warehouse = {};
	this.once_at = {}
	
	this.film = 1;
}
system.prototype.controls = controls
system.prototype.change_occurred = change_occurred
system.prototype.stop_monitoring = stop_monitoring
system.prototype.demo = demo

export async function make_system (inaugural_parameters) {
	const this_system = new system ();
	const these_controls = this_system.controls ();
	
	async function make (parameters) {
		if (this_system.film >= 1) { 
			console.log ("system: received parameters", this_system.parameters) 
		}

		these_controls.activities = {}
		
		let fields = [];
		try {
			let fields = Object.keys (parameters)
		}	
		catch (exception) {
			throw new Error (exception)
		}
				
		for (const parameter in parameters) {		
			if (parameter === "warehouse") {
				this_system.warehouse = await parameters [ parameter ] ()
			}
			
			if (parameter === "activities") {				
				const activities = parameters [ parameter ]
				for (const $circuit in activities) {
					if (this_system.film >= 1) { console.log ("system: adding $circuit", $circuit) }
					
					these_controls.activities [ $circuit ] = async function (outer_parameters) {
						if (this_system.film >= 1) { 
							console.log (`system: $circuit "${ $circuit }" called`) 
						}
												
						return await activities [ $circuit ] ({
							change: these_controls.change,
							warehouse: async function (field) {
								if (this_system.film >= 1) { console.log ("system: warehouse from $circuit") }
								
								const warehouse = cloneDeep (this_system.warehouse)
																
								if (field === undefined) {
									return warehouse
								}
								
								return warehouse [ field ]
							},
							activities: these_controls.activities
						}, outer_parameters)
					}
				}
			}
			
			if (parameter === "once_at") {
				this_system.once_at = parameters [ parameter ]
			}
		}
				
		if (has_field (this_system.once_at, "start")) {
			return await this_system.once_at.start ({
				warehouse: async function (field) {
					if (this_system.film >= 1) { console.log ("system: warehouse from @start") }
					
					const warehouse = cloneDeep (this_system.warehouse)
								
					if (field === undefined) {
						return warehouse
					}
					
					return warehouse [ field ]
				},
				activities: these_controls.activities
			})
		}
	}	
	
	await make (inaugural_parameters);
	
	return these_controls;
}
