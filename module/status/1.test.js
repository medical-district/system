



/*
	await WAIT (1000)	
*/
const WAIT = async function (DURATION) {
	await new Promise (F => {
		setTimeout (() => {
			F ()
		}, DURATION)
	})
}


import { make_system } from './../index.js'
	
import { describe, it, expect } from 'vitest'
import assert from 'assert'

describe ('system', () => {
	it ('functions', async () => {
		let notifications = 0;
		
		const system = await make_system ({
			warehouse: async function () {
				return {}
			},
			once_at: {
				async start () {}
			},
			activities: {}			
		})		
		
		const monitor = system.monitor (function ({ inaugural }) {
			const warehouse = system.warehouse ()
			console.log ("monitor was established", inaugural, warehouse)
			
			notifications += 1;
		})	
		assert.equal (system.monitor_count (), 1)
	
	
		const warehouse = system.warehouse ()
		assert.deepEqual (warehouse, {})

		monitor.stop ()
		assert.equal (system.monitor_count (), 0)
	})
		
	it ('can be customized', async () => {
		let notifications = 0;
		let started_called = 0;
		
		const system = await make_system ({
			
			/*
				THE warehouse IS THE inaugural THING THAT 
				IS BUILT WHEN THE system IS BEING MADE.
			*/
			warehouse: async function () {
				return {
					S: 1
				}
			},

			activities: {
				async activity_1 ({ change, warehouse }, { repeat }) {	
					console.log ("activity 1, repeat", repeat, await warehouse ())
	
					await WAIT (500)
					console.log ("activity 1, after wait.")
					
					let S = (await warehouse ()).S
					console.log ({ S })
					
					assert.deepEqual (await warehouse (),	{ S: repeat  })
					await change ("S", S + 1)
					assert.deepEqual (await warehouse (),	{ S: 1 + repeat })
					
					// await WAIT (500)
					
					console.log ("activity 1, after change", { warehouse })
				}
			},
			
			once_at: {
				async start ({ activities, warehouse }) {
					assert.deepEqual (await warehouse (), { S: 1 })
					
					await WAIT (500)
					
					console.log ("started", { activities })
					
					assert.deepEqual (await warehouse (), { S: 1 })
					await activities.activity_1 ({ repeat: 1 })
					assert.deepEqual (await warehouse (), { S: 2 })
					
					started_called++;
				}
			}			
		})	

		assert.equal (started_called, 1)	
		assert.equal (notifications, 0)
		
		//
		//	monitor TO THE system
		//		
		const monitor = system.monitor (function () {
			const warehouse = system.warehouse ()
			console.log ("monitor function was called", warehouse)
			
			notifications += 1;
		})	
		
		// monitor function is called once at inaugeration
		assert.equal (notifications, 1)
		assert.equal (system.monitor_count (), 	1)
		assert.deepEqual (system.warehouse (), 			{ S: 2 })

		
		//
		//	start an activity
		//
		await system.activities.activity_1 ({ repeat: 2 })
		assert.deepEqual (system.warehouse (), { S: 3 })
		
		//
		// monitor FUNCTION IS CALLED EVERY TIME A PLAY IS MAKE
		//
		assert.equal (notifications, 2)
	

		monitor.stop ()
		assert.equal (system.monitor_count (), 0)
	})
})