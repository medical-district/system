

/*
 * 		yarn run vitest run "status/3.test.js"
 */



/*
	await wait (1000)	
*/
const wait = async function (DURATION) {
	await new Promise (F => {
		setTimeout (() => {
			F ()
		}, DURATION)
	})
}


import { make_system } from './../index.js'
	
import { describe, it, expect } from 'vitest'
import assert from 'assert'

describe ('system', () => {
	it ('functions', async () => {
		let notifications = 0;
		
		var demos = 0;
		const system = await make_system ({
			warehouse: async function () {
				return {}
			},
			once_at: {
				async start () {},
				async demo () {
					console.log ("demo!");
					
					demos += 1
				}
			},
			activities: {}			
		})		
		
		const monitor = system.monitor (function ({ inaugural }) {
			const warehouse = system.warehouse ()
			console.log ("monitor was established", inaugural, warehouse)
			
			notifications += 1;
		})	
		assert.equal (system.monitor_count (), 1)
	
		const warehouse = system.warehouse ()
		assert.deepEqual (warehouse, {})

		monitor.stop ()
		assert.equal (system.monitor_count (), 0)
		
		await system.demo ()
		
		assert.equal (demos, 1)
	})

})