

/*
	yarn run vitest status/readme.test.js
*/


import { make_system } from './../index.js'
	
import { describe, it, expect } from 'vitest'
import assert from 'assert'



describe ('system', () => {
	it ('functions', async () => {
		
		function is_equal (v1, v2) {
			if (v1 === v2) {
				return true;
			}
			throw new Error (`v1 '${ v1 }' does not equal v2 '${ v2 }'`)
		}
		
		
		/*
			These functions are called only once,
			in this sequence, after "make_system" is called:
			
				warehouse
				once_at.start
		*/
		const system = await make_system ({
			warehouse: async function () {
				console.log ("warehouse function called")
				
				return {
					status: 7
				}
			},

			activities: {
				async activity_1 (
					{ change, warehouse }, 
					{ numeral }
				) {
					let status = (await warehouse ()).status			
					await change ("status", status * numeral)
					
					return "good";
				}
			},
			
			once_at: {
				async start ({ activities, warehouse }) {
					console.log ("start function called")					
				}
			}			
		})	

		/*
			system monitoring:
			
			notes:
				The monitor function is called once when
				setup, with "inaugural" == true.
			
				The monitor function is called every time
				an activity calls "change", such as:
				
					await change ("status", 998)
		*/		
		const monitor = system.monitor (({ inaugural, field }) => {
			const warehouse = system.warehouse ()

			console.log ('monitor function', { inaugural, field, warehouse })
		})

		
		//
		// After subscribing, the subscriber count is 1
		is_equal (system.monitor_count (), 1)


		//
		// activities
		const activity_1_proceeds = await system.activities.activity_1 ({ numeral: 10 })
		is_equal (activity_1_proceeds, "good") 
		is_equal (system.warehouse ().status, 70) 
	

		monitor.stop ()
		is_equal (system.monitor_count (), 0)
	})
})